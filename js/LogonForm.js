//-----------------------------------------------------------------
// Licensed Materials - Property of IBM
//
// WebSphere Commerce
//
// (C) Copyright IBM Corp. 2007, 2014 All Rights Reserved.
//
// US Government Users Restricted Rights - Use, duplication or
// disclosure restricted by GSA ADP Schedule Contract with
// IBM Corp.
//-----------------------------------------------------------------

/** 
 * @fileOverview This javascript is used by UserRegistrationAddForm.jsp and CheckoutLogon.jsp.
 * @version 1.0
 */

  /* Import dojo classes. */
dojo.require("wc.service.common");

/**
 *  The functions defined in the class helps the customer to register with the store. Another function enables a returning 
 *  customer to Sign in for quickcheckout from the shopping cart page.
 *
 *  @class This LogonForm class defines all the functions and variables used to validate the information provided by the
 *	customer to register with the store.To register, a customer creates a logon ID and password. Then, the customer provides their first name, 
 *  last name, street address, city, country/region, state/province, ZIP/postal code, e-mail address and phone number. Other registration options 
 *  include promotional e-mails, preferred language and currency, age, gender, and the remember me option. 
 */
LogonForm ={
	sslPort: 80,
	setSSLPortParameter:function(sslPort){
		this.sslPort = sslPort;
		cursor_clear();
	},	
	/**
	 * This function validates the logon ID and password for returning customers to sign in and complete the checkout process.
	 * @param {string} form The name of the form containing logon ID and password fields.
	 */
	SubmitAjaxLogin:function(form){
		reWhiteSpace = new RegExp(/^\s+$/);
		var labelLogonId = form.labelLogonId.value;
		var labelLogonPassword = form.labelLogonPassword.value;
		
		if(form.logonId != null && reWhiteSpace.test(form.logonId.value) || form.logonId.value == "" || form.logonId.value == labelLogonId){ 
			MessageHelper.formErrorHandleClient(form.logonId.id,MessageHelper.messages["ERROR_LogonIdEmpty"]);
			return false;
		}
		
		var isEmail = MessageHelper.isValidEmail(form.logonId.value);
		if(!isEmail){
			var isRUT = AddressHelper.validateRut(form.logonId.value);
			if(!isRUT){
				MessageHelper.formErrorHandleClient(form.logonId.id,MessageHelper.messages["ERROR_LogonIdInvalid"]);
				return false;
			}
		}
		
		if(form.logonPassword != null && reWhiteSpace.test(form.logonPassword.value) || form.logonPassword.value == "" || form.logonPassword.value == labelLogonPassword){ 
			MessageHelper.formErrorHandleClient("passwordError",MessageHelper.messages["ERROR_PasswordEmpty"]);
			return false;
		}
		
		/*For Handling multiple clicks. */
		if(!submitRequest()){
			return false;
		}
		setDeleteCartCookie();
				
		form.submit();	

	},
	
	/** 
	 * This function is called when the Submit button is clicked on the Registration page. All the fields containing customer
	 * information are validated and PersonProcessServicePersonRegistration is called. 
	 * @param {string} form The name of the registration form containing all the customer information.
	 */
	prepareSubmit:function (form)
	{
		if (!this.validatePrepareForm(form)){
			return;
		}
		/* For Handling multiple clicks. */
		if(!submitRequest()){
			return;
		}
		/* GMOSTACERO: ABCDIN - 20150317 - SE CAMBIA VISTA DE REGISTRO - INICIO*/
		//dojo.cookie("WC_SHOW_USER_ACTIVATION_" + WCParamJS.storeId, "true", {path:'/'});
		/* GMOSTACERO: ABCDIN - 20150317 - SE CAMBIA VISTA DE REGISTRO - FIN*/
		form.submit();
	},
	/** 
	 * GMOSTACERO: ABCDIN - 20150604 - CSR Registro de Usuario
	 * @param {string} form The name of the registration form containing all the customer information.
	 */
	prepareCSRSubmit:function (form)
	{
		if (!this.validatePrepareCSRForm(form)){
			return;
		}
		/* For Handling multiple clicks. */
		if(!submitRequest()){
			return;
		}
		form.submit();
	},
	/**
	 * The function is called to validate and prepare the form.
	 * @param {string} form The name of the registration form containing all the customer information.
	 * @return (Boolean) true if the form is ready to submit, false otherwise.
	 */
	validatePrepareForm: function(form){
		reWhiteSpace = new RegExp(/^\s+$/);
		
		if(form.logonId != null && reWhiteSpace.test(form.logonId.value) || form.logonId.value == ""){
			MessageHelper.abcdinFormErrorHandleClient(form.logonId.id,"ERROR_LogonIdEmpty"); 
			return false;
		} else {
			//GMOSTACERO: ABCDIN - 20150323 - VALIDACION DE CORREO PARA LOGONID - INICIO
			if(!MessageHelper.isValidEmail(form.logonId.value)){
				MessageHelper.abcdinFormErrorHandleClient(form.logonId.id, "ERROR_INVALIDEMAILFORMAT");
				return false;
			}
			//GMOSTACERO: ABCDIN - 20150323 - VALIDACION DE CORREO PARA LOGONID - FIN
		}

		if (form.ancestorOrgs != null) {
			if (form.ancestorOrgs.value == "") {
				MessageHelper.abcdinFormErrorHandleClient(form.ancestorOrgs.id,"ERROR_OrgNameEmpty");
				return false;
			} else if (form.ancestorOrgs.value == "Default Organization") {
				MessageHelper.abcdinFormErrorHandleClient(form.ancestorOrgs.id,"ERROR_DefaultOrgRegistration");
				return false;
			}
		}
	         
		if(form.logonPassword != null && reWhiteSpace.test(form.logonPassword.value) || form.logonPassword.value == ""){ 
			MessageHelper.abcdinFormErrorHandleClient(form.logonPassword.id,"ERROR_PasswordEmpty"); 
			return false;}
		if(form.logonPasswordVerify != null && reWhiteSpace.test(form.logonPasswordVerify.value) || form.logonPasswordVerify.value == ""){ 
			MessageHelper.abcdinFormErrorHandleClient(form.logonPasswordVerify.id,"ERROR_VerifyPasswordEmpty"); 
			return false;}
		if(form.logonPassword.value!= form.logonPasswordVerify.value){ 
			MessageHelper.abcdinFormErrorHandleClient(form.logonPasswordVerify.id,"PWDREENTER_DO_NOT_MATCH");
			return false;
		}
		
		/** Uses the common validation function defined in AddressHelper class for validating first name, 
		 *  last name, street address, city, country/region, state/province, ZIP/postal code, e-mail address and phone number. 
		 */
		if(!AddressHelper.validateAddressForm(form)){
			return false;
		}
		
		/* Checks whether the customer has registered for promotional e-mails. */
		if(form.sendMeEmail && form.sendMeEmail.checked){
		    form.receiveEmail.value = true;
		}
		else {
			form.receiveEmail.value = false;
		}
		
		if(form.sendMeSMSNotification &&  form.sendMeSMSNotification.checked){
		    form.receiveSMSNotification.value = true;
		}
		else {
			form.receiveSMSNotification.value = false;
		}
		
		if(form.sendMeSMSPreference && form.sendMeSMSPreference.checked){
		    form.receiveSMS.value = true;
		}
		else {
			form.receiveSMS.value = false;
		}
		
		if(form.mobileDeviceEnabled != null && form.mobileDeviceEnabled.value == "true"){
			if(!MyAccountDisplay.validateMobileDevice(form)){
				return false;
			}
		}
		/*
		if(form.birthdayEnabled != null && form.birthdayEnabled.value == "true"){
			if(!MyAccountDisplay.validateBirthday(form)){
				return false;
			}
		}*/
		return true;
	},
	
	showCustomMessage: function(formFieldId, message){
		
		var divRegistroCompleto = document.getElementById("mensajeErrorRegitroCompleto");
		
		if(divRegistroCompleto !=null){
			$('#mensajeErrorRegitroCompleto').empty();
			$('#mensajeErrorRegitroCompleto').show();
			$('#mensajeErrorRegitroCompleto').html(message);
		}else{
			MessageHelper.formErrorHandleClient(formFieldId, message);
		}
	},
	/**
	 * The function is called to validate and prepare the form by CSR.
	 * @param {string} form The name of the registration form containing all the customer information.
	 * @return (Boolean) true if the form is ready to submit, false otherwise.
	 */
	validatePrepareCSRForm: function(form){
		reWhiteSpace = new RegExp(/^\s+$/);
		if(form.logonId != null && reWhiteSpace.test(form.logonId.value) || form.logonId.value == ""){ 
			MessageHelper.formErrorHandleClient(form.logonId.id,MessageHelper.messages["ERROR_LogonIdEmpty"]);			
			return false;
		} else {
			//GMOSTACERO: ABCDIN - 20150323 - VALIDACION DE CORREO PARA LOGONID - INICIO
			if(!MessageHelper.isValidEmail(form.logonId.value)){
				MessageHelper.formErrorHandleClient(form.logonId.id, MessageHelper.messages["ERROR_INVALIDEMAILFORMAT"]);
				return false;
			}
			//GMOSTACERO: ABCDIN - 20150323 - VALIDACION DE CORREO PARA LOGONID - FIN
		}

		if (form.ancestorOrgs != null) {
			if (form.ancestorOrgs.value == "") {
				MessageHelper.formErrorHandleClient(form.ancestorOrgs.id,MessageHelper.messages["ERROR_OrgNameEmpty"]);
				return false;
			} else if (form.ancestorOrgs.value == "Default Organization") {
				MessageHelper.formErrorHandleClient(form.ancestorOrgs.id,MessageHelper.messages["ERROR_DefaultOrgRegistration"]);
				return false;
			}
		}
	         
		/** Uses the common validation function defined in AddressHelper class for validating first name, 
		 *  last name, street address, city, country/region, state/province, ZIP/postal code, e-mail address and phone number. 
		 */
		if(!AddressHelper.validateAddressForm(form)){
			return false;
		}
		
		/* Checks whether the customer has registered for promotional e-mails. */
		if(form.sendMeEmail && form.sendMeEmail.checked){
		    form.receiveEmail.value = true;
		}
		else {
			form.receiveEmail.value = false;
		}
		
		if(form.sendMeSMSNotification &&  form.sendMeSMSNotification.checked){
		    form.receiveSMSNotification.value = true;
		}
		else {
			form.receiveSMSNotification.value = false;
		}
		
		if(form.sendMeSMSPreference && form.sendMeSMSPreference.checked){
		    form.receiveSMS.value = true;
		}
		else {
			form.receiveSMS.value = false;
		}
		
		if(form.mobileDeviceEnabled != null && form.mobileDeviceEnabled.value == "true"){
			if(!MyAccountDisplay.validateMobileDevice(form)){
				return false;
			}
		}
		
		if(form.birthdayEnabled != null && form.birthdayEnabled.value == "true"){
			if(!MyAccountDisplay.validateBirthday(form)){
				return false;
			}
		}
		return true;
	},
		
	/**
	 *  This function validates the customer's input for age. If the user is under age, pop up a message to ask the user to review the store policy.
	 *  @param {string} The name of the form containing personal information of the customer.
	 */
	validateAge: function(form){
		
		var birth_year = parseInt(form.birth_year.value);
		var birth_month = parseInt(form.birth_month.value);
		var birth_date = parseInt(form.birth_date.value);
		
		if (birth_year == 0 || birth_month == 0 || birth_date == 0) {
			return;
		}
		
		var curr_year = parseInt(form.curr_year.value);
		var curr_month = parseInt(form.curr_month.value);
		var curr_date = parseInt(form.curr_date.value);
		
		/*Check whether age is less than 13, if so, pop up a message to ask the user to review the store policy. */
		if((curr_year - birth_year) < 13){
			MessageHelper.formErrorHandleClient(form.birth_date.id, MessageHelper.messages["BIRTHDAY_ERROR_DATE"]);
			return false;
		//	alert(MessageHelper.messages["BIRTHDAY_ERROR_DATE"]);
		}else if((curr_year - birth_year) == 13){
			if(curr_month < birth_month){
				MessageHelper.formErrorHandleClient(form.birth_date.id, MessageHelper.messages["BIRTHDAY_ERROR_DATE"]);
				return false;
			//	alert(MessageHelper.messages["BIRTHDAY_ERROR_DATE"]);
			}else if((curr_month == birth_month) && (curr_date < birth_date)){
				MessageHelper.formErrorHandleClient(form.birth_date.id, MessageHelper.messages["BIRTHDAY_ERROR_DATE"]);
				return false;
				//alert(MessageHelper.messages["BIRTHDAY_ERROR_DATE"]);
			}
		}
	},
	
	/**
	  *	This function is used when "Age" option is changed.
	  * This will show one alert message if the user age is under 13.
	  * @param {string} form The name of the registration form containing customer's age.
	  */

	checkAge:function (form)
	{
		if(form.age.value==1) {
			alert(MessageHelper.messages["AGE_WARNING_ALERT"]);
		}
	},
	/** 
	 * This function is called when the Submit button is clicked on the Quick Registration form customer
	 * information are validated and PersonProcessServicePersonRegistration is called. 
	 * @param {string} form The name of the registration form containing all the customer information.
	 */
	prepareQuickRegisterSubmit:function (form){
		if (!AbcdinMyAccountDisplay.validateQuickRegisterPrepareForm(form)){
			return;
		}
		var params = {};
		params["logonId"] = form["logonId"].value;
		params["logonPassword"] = form["logonPassword"].value;
		params["storeId"] = form["storeId"].value;
		params["langId"] = form["langId"].value;
		params["catalogId"] = form["catalogId"].value;
		params["page"] = form["eventPage"].value;
		wc.service.invoke("AjaxUserQuickRegistrationValidateView", params);
	},
	/** 
	 * This function is called when the Submit button is clicked on the Quick Registration form customer
	 * information are validated and PersonProcessServicePersonRegistration is called. 
	 * @param {string} form The name of the registration form containing all the customer information.
	 */
	prepareResendPasswordSubmit:function (form){
		reWhiteSpace = new RegExp(/^\s+$/);
		var labelLogonId = form.labelLogonId.value;
		var labelLogonPassword = form.labelLogonPassword.value;
		
		if(form.logonId != null && reWhiteSpace.test(form.logonId.value) || form.logonId.value == "" || form.logonId.value == labelLogonId){ 
			MessageHelper.formErrorHandleClient(form.logonId.id,MessageHelper.messages["ERROR_LogonIdEmpty"]);
			return false;
		}
		
		var isEmail = MessageHelper.isValidEmail(form.logonId.value);
		if(!isEmail){
			var isRUT = AddressHelper.validateRut(form.logonId.value);
			if(!isRUT){
				MessageHelper.formErrorHandleClient(form.logonId.id,MessageHelper.messages["ERROR_LogonIdInvalid"]);
				return false;
			}
		}
		
		if(form.logonPassword != null && reWhiteSpace.test(form.logonPassword.value) || form.logonPassword.value == "" || form.logonId.value == labelLogonPassword){
			MessageHelper.formErrorHandleClient("errorPassword",MessageHelper.messages["ERROR_PasswordEmpty"]);
			return false;
		}
		
		/* For Handling multiple clicks. */
		if(!submitRequest()){
			return;
		}
		form.submit();
	},
	prepareResendChangePasswordSubmit:function (form){
		reWhiteSpace = new RegExp(/^\s+$/);
		if(form.validationCode != null && reWhiteSpace.test(form.validationCode.value) || form.validationCode.value == ""){ 
			MessageHelper.formErrorHandleClient(form.validationCode.id,MessageHelper.messages["ERROR_LogonIdEmpty"]); 
			return false;
		}
		if(form.logonPassword != null && reWhiteSpace.test(form.logonPassword.value) || form.logonPassword.value == ""){ 
			MessageHelper.formErrorHandleClient(form.logonPassword.id,MessageHelper.messages["ERROR_PasswordEmpty"]); return false;}
		if(form.logonPasswordVerify != null && reWhiteSpace.test(form.logonPasswordVerify.value) || form.logonPasswordVerify.value == ""){ 
			MessageHelper.formErrorHandleClient(form.logonPasswordVerify.id,MessageHelper.messages["ERROR_VerifyPasswordEmpty"]); return false;}
		if(form.logonPassword.value!= form.logonPasswordVerify.value){ 
			MessageHelper.formErrorHandleClient(form.logonPasswordVerify.id,MessageHelper.messages["PWDREENTER_DO_NOT_MATCH"]);
			return false;
		}
		
		/* For Handling multiple clicks. */
		if(!submitRequest()){
			return;
		}
		form.submit();
	},
	prepareRepresentativeSubmit:function (form){
		/* For Handling multiple clicks. */
		if(!submitRequest()){
			return;
		}
		var params = {};
		params["logonId"] = form["logonId"].value;
		params["URL"] = form["URL"].value;
		params["URLHome"] = form["URLHome"].value;
		params["storeId"] = form["storeId"].value;
		params["langId"] = form["langId"].value;
		params["catalogId"] = form["catalogId"].value;
		params["sslPort"] = this.sslPort;
		wc.service.invoke("AjaxUserRepresentativeView", params);
	},
	representativeUserById:function (form){
		/* For Handling multiple clicks. */
		if(!submitRequest()){
			return;
		}
		//var host = window.location.hostname;
		//var port = this.sslPort;
		var path = form.URL.value;
		var runAsUserId = "&runAsUserId=" + form.userId.value;
		//var storeId = "&storeId=" + form.storeId;
		//var langId = "&langId=" + form.langId;
		//var catalogId = "&catalogId=" + form.catalogId;
		var urlHomeView = "&URL=AjaxLogonForm";
		//var url = "https://" + host + ":" + port + path + runAsUserId + storeId + langId + catalogId + urlHomeView; 
		var url = path + runAsUserId + urlHomeView;
		
		document.location.href = url ;
	}
};